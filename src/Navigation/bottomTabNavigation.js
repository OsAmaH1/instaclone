
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
  
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Foundation from 'react-native-vector-icons/Foundation';

import Discovery from '../screens/DiscoveryScreen'
import Post from '../screens/PostScreen'
import Notification from '../screens/NotificationScreen'
import Profile from '../screens/ProfileScreen'
import HomeStackScreen from './homestack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

const TabNavigation = ()=>{
    return(
<Tab.Navigator>
        <Tab.Screen name="Home"
        options={{
          tabBarLabel:'',
          
          tabBarIcon: ({ color='orange', size=12 }) => (
            <Foundation name="home" color={color} size={size} />
          ),
        }} 
        component={HomeStackScreen} />
        <Tab.Screen name="Discovery" 
        options={{
          tabBarLabel:'',
          tabBarIcon: ({ color='orange', size=12 }) => (
            <Ionicons name="search-outline" color={color} size={size} />
          ),
        }}
         component={Discovery} />
        <Tab.Screen name="Post"
        options={{
          tabBarLabel:'',
          tabBarIcon: ({ color='orange', size=12 }) => (
            <Ionicons name="add-circle-outline" color={color} size={size} />
          ),
        }} component={Post} />
        <Tab.Screen name="Notification"
        options={{
          tabBarLabel:'',
          tabBarIcon: ({ color='orange', size=12 }) => (
            <Ionicons name="heart-outline" color={color} size={size} />
          ),
        }}
         component={Notification} />
        <Tab.Screen name="Profile"
        options={{
          tabBarLabel:'',
          tabBarIcon: ({ color='orange', size=12 }) => (
            <Ionicons name="person-outline" color={color} size={size} />
          ),
        }}
         component={Profile} />
      </Tab.Navigator>
    
    )
}


export default TabNavigation