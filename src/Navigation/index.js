
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import bottomTabNavigation from './bottomTabNavigation'
import StoryScreenPreview from '../screens/StoryScreen';
const RootStack = createStackNavigator();

const RootNavigation = ()=>{
    return(
<RootStack.Navigator>
        <RootStack.Screen name="Home"
        component={bottomTabNavigation} 
        options={{
          headerShown:false
        }}
        />

<RootStack.Screen 
      name='StoryPreview' 
      component={StoryScreenPreview}
      options={{
        headerShown:false
}}
        />
       
      </RootStack.Navigator>
    
    )
}


export default RootNavigation