import React from 'react';
import {
  View,
  Image
} from 'react-native';
import HomeScreen from "../screens/Homescreen"
import { createStackNavigator } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';





const HomeStack = createStackNavigator();

const HomeStackScreen = ()=>{
    return (
        <HomeStack.Navigator>
          <HomeStack.Screen options={{
            title:'Instagram',
            headerTitleAlign:'center',
            headerLeft: () =>
            <View style={{marginLeft:10}}>
               <Ionicons name={Platform.OS === "ios" ? "md-camera-outline" : "md-camera-outline"} 
            color="#000" size={30} />
            </View>
            ,
            headerTitle:()=>
              <Image  source={{uri:'https://www.citypng.com/public/uploads/small/11590321744a33mnrip0urw5ag5t9jz1rjuccygn57cb4e0nb4vqosrk4n3sjrjkh8i0oxsulxq64o6msvhhlgbenatvb7kzsymdyvizi2smlak.png'}}
              style={{resizeMode:'contain' , width:120 , height:100}}/>
               ,
            headerRight:() =>
            <View style={{marginRight:10}}>
               <Ionicons name={Platform.OS === "ios" ? "paper-plane-outline" : "paper-plane-outline"} 
            color="#000" size={30} />
            </View>
    }
          } name="Home" component={HomeScreen} />
        </HomeStack.Navigator>
      );


}


export default HomeStackScreen