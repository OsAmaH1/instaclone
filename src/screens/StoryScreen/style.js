import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container:{
     height:'100%',
     justifyContent:'center'
    },
    image:{
        flex:1,
        justifyContent:'space-between',
        resizeMode:'contain'
    },
    userinfo:{
        flexDirection:"row",
        alignItems:'center',
        marginTop:10
    },
    text:{
     color:"#fff",
     fontWeight:'500',
     fontSize:18
    },
    input:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:20

    },
    textinput:{
       borderColor:'#fff',
       color:'#fff',
       flex:1,
       borderWidth:1,
       borderRadius:50,
       marginHorizontal:15,
      
    },
    camera:{
        width:40,
        borderWidth:1,
        borderRadius:50,
        justifyContent:'center',
        borderColor:'#fff',
        alignItems:'center',
        height:40
        


        
    }


})

export default styles;