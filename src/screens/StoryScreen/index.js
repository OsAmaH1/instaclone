import React , {useEffect, useState} from 'react'
import { View , Text , SafeAreaView , ImageBackground, ActivityIndicator , TouchableWithoutFeedback, Dimensions, TextInput} from 'react-native'
import {useNavigation, useRoute } from '@react-navigation/native'
import StoriesData from '../../Data/Storiesdata'
import ProfilePicture from '../../components/ProfilePicture'
import Styles from './style'
import Ionicons from 'react-native-vector-icons/Ionicons';

const StoryScreenPreview = ()=>{
    const route = useRoute();
    const navigation = useNavigation();
    const [userStories, setUserStories] = useState(null)
    const [activeStoryIndex, setActiveStoryIndex] = useState(null)
    
    
    const userId = route.params.userId;
    useEffect(()=>{
        
        const userStories = StoriesData.find(StoryData => StoryData.user.id === userId)
        setUserStories(userStories)
        setActiveStoryIndex(0)
   
    }, [])

   const  naviagteToNextUser  = ()=>{
       navigation.push('StoryPreview', {userId:(parseInt(userId) + 1).toString()})
   }
   const naviagteToPrevUser = ()=>{
    navigation.push('StoryPreview', {userId:(parseInt(userId) - 1).toString()})
   }

    const handleNextStory = ()=> {
        if(activeStoryIndex >= userStories.stories.length - 1){
            naviagteToNextUser()
            return;
        }
        setActiveStoryIndex(activeStoryIndex + 1 )
    }
 
    const handlePrevStory = ()=>{
        if(activeStoryIndex <= 0){
            naviagteToPrevUser()
      return;
        }
     setActiveStoryIndex(activeStoryIndex - 1)
    } 
 
    const  handlePress = (evt) =>{
         const x = evt.nativeEvent.locationX;
         const screenWidth = Dimensions.get('window').width
 
 
        if (x < screenWidth / 2){
            handlePrevStory()
        }
        else{
            handleNextStory()
        }
    }
        



       if(!userStories){
      return ( <SafeAreaView>
         <ActivityIndicator/>    
    </SafeAreaView>)
    }


    const activeStory = userStories.stories[activeStoryIndex];

 return(
     <SafeAreaView style={Styles.container}>
         <TouchableWithoutFeedback onPress={handlePress}> 
         <ImageBackground style={Styles.image} source={{uri:activeStory.imageUri}}>
         <View style={Styles.userinfo}>
         <ProfilePicture uri={userStories.user.imageUri} size={50}/>
         <Text style={Styles.text}>{userStories.user.name}</Text>
          
        </View>
        <View style={Styles.input}>
            <View style={Styles.camera}><Ionicons name= "md-camera-outline"  color="#fff" size={30}  /></View>
            
            <TextInput placeholder='Write your message' placeholderTextColor="#fff" style={Styles.textinput}/>
            <Ionicons name= "paper-plane-outline"  color="#fff" size={35} />
        </View>
        </ImageBackground>
         </TouchableWithoutFeedback>
          
     </SafeAreaView>
     
 )
}


export default StoryScreenPreview