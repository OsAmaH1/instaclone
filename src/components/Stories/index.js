import React from 'react'
import { View, FlatList} from 'react-native'
import Story from '../Story'
import Styles from './stories.style'
import StoriesData from '../../Data/Storiesdata'



const Stories =()=>{
    return(

        <FlatList 
        data={StoriesData}
        horizontal
        showsHorizontalScrollIndicator={false}
        style={Styles.container}
        keyExtractor={({user:{id}})=> id} renderItem={({item})=>  <Story  story={item} 
        
        />} />
     
    )
}


export default Stories