
import React from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity
  
} from 'react-native';
import Styles from './styles'
import ProfilePicture from '../ProfilePicture'
import Stories from '../Stories';
import {useNavigation} from '@react-navigation/native'





const Story = (props) => {


  const {
    story:{
      user:{
        id,
        imageUri,
        name
      }
    }
  } = props

   const Navigation = useNavigation();
  const onpress =()=>{
    Navigation.navigate('StoryPreview' , {userId: id})
  } 
  return (
      <TouchableOpacity style={Styles.container} onPress={onpress}>
         <ProfilePicture uri={imageUri}/>
         <Text style={Styles.nametext}>{name}</Text>
      </TouchableOpacity>
   
  );
};

export default Story;
