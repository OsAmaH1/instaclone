import React from 'react'
import { View , Text} from 'react-native'
import ProfilePicture from '../../../ProfilePicture'
import Styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons';


const Header = ({imageUri, name }) => {
    return(
            <View style={Styles.container}>  
            <View style={Styles.left}>
                  <ProfilePicture uri={imageUri} size={35}/>
                 <Text style={Styles.name}>{name}</Text>
                 </View>
                 <View style={Styles.left}>
                 <Icon name="ellipsis-vertical"  size={16} style={Styles.icon} />
                 </View>
                
                 
            </View>
             )
}
export default Header;