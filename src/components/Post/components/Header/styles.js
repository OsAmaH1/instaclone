import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container:{
      flexDirection:"row",
      justifyContent:'space-between'

    },
    left:{
     flexDirection:"row"
    },
    name:{
       alignSelf:'center',
       fontWeight:'bold',
       color:'#3c3c3c' 
    },
    icon:{
      alignSelf:'center',
      color:'#000',
      marginRight:15,
    
     
    }


})

export default styles;