import React , {useState, useEffect} from 'react'
import { View, Text , TouchableOpacity} from 'react-native'
import Styles from './footer.style'
import Icon from 'react-native-vector-icons/Ionicons';


const Footer = ({likecount:likesCountprop, caption, postedAt})=>{

  const [isheart , setIsHeart] = useState(false);
  const [likecount , setLikesCount] = useState(0)
  const [isSaved , setIsSaved] = useState(false);
 
 
 
  const isheartHandler = () =>{
    const amount = isheart ? -1 : 1;
    setLikesCount(likecount + amount);
    setIsHeart(!isheart);
  }
  const isSavedHandler = ()=>{
    setIsSaved(!isSaved);
  }

  useEffect( () =>{
      setLikesCount(likesCountprop);
  }, [])




    return(
        <View style={Styles.container}>
          <View  style={Styles.iconContainer}>
            <View style={Styles.left}>
              <TouchableOpacity onPress= {isheartHandler} >
              {!isheart ? <Icon  size={25} name="heart-outline" style={Styles.icon} /> : <Icon  size={25}  name="heart-sharp" color='red' />}
              </TouchableOpacity>
            
              <Icon name="chatbubble-outline" size={25} style={Styles.icon} />
              <Icon name="paper-plane-outline" size={25} style={Styles.icon} />
          </View>
          <TouchableOpacity onPress= {isSavedHandler} >
       {!isSaved ? <Icon name="bookmark-outline" size={25} style={Styles.icon} /> : <Icon name="bookmark-sharp" size={25} color='black' /> }   
          </TouchableOpacity>
          </View>
          <Text style={Styles.likes}> {likecount} likes </Text> 
          <Text style={Styles.caption}> {caption} </Text> 
          <Text style={Styles.postedAt}> {postedAt}  </Text> 
        </View>
        
    )
}


export default Footer;