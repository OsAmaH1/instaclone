import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container:{
    margin:5
    },
    likes:{
        fontWeight:'bold',
        margin:3
    },
    caption:{
        margin:3

    },
    postedAt:{
       color:'#8c8c8c',
       margin:3
    },
    iconContainer:{
        flexDirection:'row',
        justifyContent:'space-between' ,
        padding:5
    },
    left:{
        flexDirection:'row',
        width:120,
        justifyContent:'space-between'
    },
    icon:{
        color:'#545454'
    }


})

export default styles;