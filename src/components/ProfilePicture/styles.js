import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container:{
        margin:7,
        borderWidth:2,
        flexDirection:'row',
        borderColor:'#9920c9',
        borderRadius:38,
    },
  image:{
   
    borderRadius:38,
    borderWidth:2,
    borderColor:'#ffffff'
   
}


})

export default styles;