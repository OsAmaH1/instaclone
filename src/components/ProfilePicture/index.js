import React from 'react';
import {
  View,
  Image
  
} from 'react-native';
import Styles from './styles'



const ProfilePicture = ({uri, size = 70}) => {
  return (
      <View style={[Styles.container, {width: size + 4 ,height:size + 4 }]}>
          <Image
            source={{uri}}
            style={[Styles.image, {width:size, height:size}]}
          />
          
      </View>
   
  );
};

export default ProfilePicture;
